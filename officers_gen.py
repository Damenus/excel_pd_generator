from faker import Factory
import random
from GeneratorPesel import pesel
from dateutil.relativedelta import relativedelta
from inhabitancy import InhabitancyContainer

fake = Factory.create("pl_PL")

class OfficerGenerator():

    inhabitancy_container = None

    def __init__(self):
        self.inhabitancy_container = InhabitancyContainer()

    # we pass badge number as be generate sample earlier
    def generate_officer(self,institusion_number, rank, office):
        officer = []
        fired = False

        # badge number - we'll add it later, in office load
        officer.append("")

        # PESEL and date
        pesel_num, birth_date = pesel(1966, 1995)
        officer.append(pesel_num)

        # first/last name
        officer.append(fake.first_name())
        officer.append(fake.last_name())

        # date of birth
        officer.append(str(birth_date))

        #office
        officer.append(office)

        #rank
        officer.append(rank)

        # date of employment/fired
        # we assume that we can employ officer if he is min 20 y old
        employment_date = fake.date_time_between(start_date=birth_date + relativedelta(years=20), end_date="now", tzinfo=None).date()
        officer.append(str(employment_date))

        # rand if officer got fired
        # modulo 17 for hugher spread
        if random.randint(0,1000) % 17 == 0:
            fired_date = fake.date_time_between(start_date=employment_date, end_date="now", tzinfo=None).date()
            officer.append(str(fired_date))
        else:   #hasnt been fired yet
            officer.append("")

        # number of institution
        officer.append(institusion_number)

        # get ingabitancy
        street, postal_code = self.inhabitancy_container.generate_inhabitancy()

        # street
        officer.append(street)

        # city
        officer.append("Gdańsk")

        # postal code
        officer.append(postal_code)

        return officer
