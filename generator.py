import xlwt
from institutions_load import load_institutions
from officers_load import load_officers


# make excel file
excel = xlwt.Workbook(encoding="utf-8")

# excel sheets names
sheets_names = ['Placówki', 'Funkcjonariusze', 'Grafik']
sheets = []

# excel columns names list
sheet_headings = []
sheet_headings.append(['Nr placówki', 'Typ Placówki', 'Nazwa', 'Ulica', 'Kod pocztowy', 'Miasto'])
sheet_headings.append(['Nr służbowy', 'PESEL', 'Imię', 'Nazwisko', 'Data urodzenia', 'Stanowisko', 'Stopień', 'Data przyjęcia', 'Data zwolnienia', 'Nr komisariatu', 'Ulica', 'Miasto', 'Kod Pocztowy'])
sheet_headings.append([])



# excel add sheets
for sheet in sheets_names:
    sheets.append(excel.add_sheet(sheet))



# add headings to sheets
for sheet,headings in zip(sheets,sheet_headings):
    i = 0
    for title in headings:
        sheet.write(0, i, title)
        i = i + 1


# ### SHEET 1 - INSTITUTIONS ###

inst_num = load_institutions(sheets[0])

# ### SHEET 2 - OFFICERS ###

load_officers(sheets[1],inst_num)


excel.save("excel_inspektora.xls")




#fake.add_provider(RankProvider)

# list = [2.34, 4.346, 4.234]



# sheet1.write(0,0,"Display")
# sheet1.write(1,0,"Dominance")
# sheet1.write(2,0,"Test")
#
# sheet1.write(0,1,x)
# sheet1.write(1,1,y)
# sheet1.write(2,1,z)
#
# sheet1.write(4,0,"Stimulus Time")
# sheet1.write(4,1,"Reaction Time")
#
# i=4
#
# for n in list:
#     i = i+1
#     sheet1.write(i,0,n)
