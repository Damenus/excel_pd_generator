from bs4 import BeautifulSoup
import urllib.request

streets_list = []

# site has 37 pages
for i in range(1,38):
    # get site
    url="https://bip.gznk.pl/zasoby/wykaz-ulic?page=" + str(i)
    page_bytes = urllib.request.urlopen(url).read()
    html_doc = page_bytes.decode("utf-8")

    soup = BeautifulSoup(html_doc, 'html.parser')

    # table with streets
    table = soup.find("table")
    tbody = table.find("tbody")
    trs = tbody.find_all("tr")

    for row in tbody.find_all("tr"):
        street = []

        # get all td elements and add them to street
        for element in row.find_all("td")[2:]:
            street.append(element.text.strip())
        streets_list.append(street)



# save to file

file = open('streets_numbers_districts', mode='w', encoding='utf-8')

for street in streets_list:
    line = street[0] + '|' + street[1] + '|' + street[2] + "\n"
    file.write(line)

file.close()